package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	ch := make(chan int)

	wg.Add(1)
	go run(ch)

	// записываем
	ch <- 555
	ch <- 666
	ch <- 555
	close(ch) // закрываем запись в канал

	wg.Wait()
}

func run(ch chan int) {
	for {
		value, ok := <-ch
		if !ok {
			fmt.Printf("Каналы отработли")
			// Как только у нас вылазит panic мы перехватывает и заканчиваем работу горутины
			wg.Done()
			return
		}
		fmt.Printf("Канал со значением: %d\n", value)
		time.Sleep(time.Second * 3) // для пинга
	}
}
